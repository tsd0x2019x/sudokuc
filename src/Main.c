/**
 * Main.c
 * 
 * This main program demonstrates how to use the library Sudoku.h to 
 * create Sodoku puzzles solver.
 * 
 */
#include "Sudoku.h"

int main (int argc, char * argv[]) {

    if (argc == 1) {
        printf("To few argument.\nUsage: ./Sudoku \"Puzzle.txt\"\n");
        exit(1);
    }

    ////
    // 1. Create/Declare a sudoku automaton
    ////
    Sudoku_Automat sudoku;

    printf("\nSudoku run...\n\n");

    ////
    // 2. Initialize the (declared) sudoku automaton and allocate memory for calculation. 
    //    The 2nd argument indicates if the automaton is a solver (SUDOKU_MODE_SOLVER) or 
    //    generator (SUDOKU_MODE_GENERATOR). The 3rd arugment is the path of the puzzle file.
    ////
    Sudoku_Init(&sudoku, SUDOKU_MODE_SOLVER, argv[1]);
    
    printf("Puzzle:\n"); // Before
    Sudoku_Print(&sudoku); // Display current state of the sudoku

    ////
    // 3. Let the sudoku automaton solve the given puzzle or generate a new puzzle.
    ////
    Sudoku_Run(&sudoku);

    printf("Solution:\n"); // After
    Sudoku_Print(&sudoku); // Display current state of the sudoku
    
    ////
    // 4. After (finished) calculation, all allocated memory spaces for the automaton 
    //    have to be released. This final step is necessary, since otherwise problem
    //    as segmentation fault could be rised.
    ////
    Sudoku_Release(&sudoku); // Release all allocated memery blocks

    return EXIT_SUCCESS;
}