/*
 * Sudoku.h
 * 
 * This header file provides necessary methods for solving and/or generating a sudoku puzzle.
 * 
 */
#ifndef SUDOKU_H
#define SUDOKU_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "LinkedList.h"

#define SUDOKU_API extern
#define SUDOKU_LINE_SIZE 9     /* Every line (side) of a block has 9 squares (1..9) */
#define SUDOKU_BLOCK_DIM 3     /* Block dimension 3. A 3x3 square block contains 9 squares. A whole sudoku has 9 such blocks. */
#define SUDOKU_BLOCK_NUMBER 9  /* Number of all 3x3-blocks in a sudoku game */
#define SUDOKU_NUMBER_SQUARES_IN_BLOCK 9 /* Number of all squares in a 3x3-block is always 9 */
#define SUDOKU_TOTAL_SIZE 81   /* A whole sudoku game has 9 lines respectively with 9 squares. Total 9x9 = 81 squares */

//=======================================================================================================================

typedef enum { 
    SUDOKU_MODE_SOLVER = 0,     // solves a given sudoku puzzle
    SUDOKU_MODE_GENERATOR = 1   // creates a (new) sudoku puzzle 
} Sudoku_Automat_Mode;

typedef struct {
    Sudoku_Automat_Mode mode; // Working mode (0 : solver, 1 : generator)
    unsigned short int * block; // 1-dimensional array representing the two-dimensional sudoku 9x9-grid
    List * arrList[SUDOKU_TOTAL_SIZE]; // Array of (List *) pointers
} Sudoku_Automat;

//=======================================================================================================================

SUDOKU_API void Sudoku_Print_Numeric (Sudoku_Automat * automat);
SUDOKU_API void Sudoku_Print_Character (Sudoku_Automat * automat);
SUDOKU_API void Sudoku_Print (Sudoku_Automat * automat);
SUDOKU_API void Sudoku_Release (Sudoku_Automat * automat);
SUDOKU_API int Sudoku_GetArrayIndexOfNthSquareInMthBlock (Sudoku_Automat * automat, int nth_square, int mth_block);
SUDOKU_API int Sudoku_CalculateIndices (Sudoku_Automat * automat, /* Kästchen-Index */ int square_index, int * row, int * col,  int * row_begin, int * row_end, int * col_begin, int * col_end, int * block_row, int * block_col, int * block_begin);
SUDOKU_API void Sudoku_UpdateRelatedSquares (Sudoku_Automat * automat, int square_index, int * count_empty_squares);
SUDOKU_API int Sudoku_EliminateDuplicateCandidates (Sudoku_Automat * automat, int square_index);
SUDOKU_API void Sudoku_SeekCandidates (Sudoku_Automat * automat, List * candiates_list, /* Kästchen-Index */ int square_index);
SUDOKU_API void Sudoku_Solve (Sudoku_Automat * automat); // We assume that the given sudoku puzzle always has a solution.
SUDOKU_API void Sudoku_Generate (Sudoku_Automat * automat);
SUDOKU_API void Sudoku_Run (Sudoku_Automat * automat);
SUDOKU_API void Sudoku_ChangeMode (Sudoku_Automat * automat, Sudoku_Automat_Mode new_mode);
SUDOKU_API void Sudoku_Init (Sudoku_Automat * automat, Sudoku_Automat_Mode mode, char * puzzle_file);

//=======================================================================================================================

/*
 * Sudoku_Print_Numeric
 * 
 * Function for displaying the current sudoku state.
 * Every square is expressed by a numeric value (0..9).
 * 
 * @param
 *      automat : Pointer to an sudoku automaton
 */
SUDOKU_API void Sudoku_Print_Numeric (Sudoku_Automat * automat) {
    for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
        fprintf(stdout, " %hu", automat->block[index]);
        if ((index + 1) % SUDOKU_LINE_SIZE == 0) {
            fprintf(stdout, "\n");
        }
    }
    printf("\n");
}

/*
 * Sudoku_Print_Character
 * 
 * Extended function for displaying the current sudoku state.
 * Every square is expressed by a character.
 * 
 * @param
 *      automat : Pointer to an sudoku automaton
 */
SUDOKU_API void Sudoku_Print_Character (Sudoku_Automat * automat) {
    for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
        char ch = (automat->block[index] == 0) ? '-' : (automat->block[index] + '0');
        fprintf(stdout, " %c", ch);
        if ((index + 1) % SUDOKU_LINE_SIZE == 0) {
            fprintf(stdout, "\n");
        }
    }
    printf("\n");
}

/*
 * Sudoku_Print
 * 
 * Displaying the current sudoku state.
 * 
 * @param
 *      automat : Pointer to an sudoku automaton
 */
SUDOKU_API void Sudoku_Print (Sudoku_Automat * automat) {
    //Sudoku_Print_Numeric(automat);
    Sudoku_Print_Character(automat);
}

/*
 * Sudoku_Release
 * 
 * Release all allocated memories spaces for the sudoku calculations.
 * 
 * @param
 *      automat : Pointer to an sudoku automaton
 */
SUDOKU_API void Sudoku_Release (Sudoku_Automat * automat) {
    if (automat->block == NULL) return;
    for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
        if (automat->block[index] != 0 && automat->arrList[index] != NULL) {
            LinkedList_ClearList(automat->arrList[index]);
            automat->arrList[index] = NULL;
        }
    }
    free(automat->block);
    automat->block = NULL;
}

/*
 * Sudoku_GetArrayIndexOfNthSquareWithinMthBlock
 * 
 * A sudoku has 9 blocks (indexed 0..8). Each block has 9 squares (numbered 0..8). Our array <automat->block>
 * representing 81 sudoku squares is an 1-dimensional array (indexed 0..80). This function maps the given square 
 * number <nth_square> and block number <mth_block> into the corresponding array index (indexed 0..80):
 * 
 * m-th block:
 *      +---+---+---+
 *      | 0 | 1 | 2 |
 *      +---+---+---+
 *      | 3 | 4 | 5 |
 *      +---+---+---+
 *      | 6 | 7 | 8 |
 *      +---+---+---+
 * 
 *  <arr_index> := 3 * [ (m / 3) * 9 + (m % 3) ] + [ (n / 3) * 9 + (n % 3) ]
 * 
 * For example: Given the 3rd block (index m=3) with its 4th square (index n=4). This function will return the 
 * array index 37. Be aware that the first block and first square begins from index 0 (and NOT 1).
 * 
 * @params
 *      automat :
 *      nth_square : Index of the square (only between 0 and 8)
 *      mth_block  : Index of the block (only between 0 and 8)
 */
SUDOKU_API int Sudoku_GetArrayIndexOfNthSquareInMthBlock (Sudoku_Automat * automat, int nth_square, int mth_block) {
    return (3 * ((mth_block / 3) * 9 + (mth_block % 3))) + ((nth_square / 3) * 9 + (nth_square % 3));
}

/*
 * Sudoku_WriteValueToNthSquareInMthBlock
 * 
 * Write a value to square with index <nth_square> in block with index <mth_block>.
 */
SUDOKU_API void Sudoku_WriteValueToNthSquareInMthBlock (Sudoku_Automat * automat, int nth_square, int mth_block, unsigned short int value) {
    // Maps the indices <nth_square> and <mth_block> into the corresponding 1-dimensional array index
    int index = Sudoku_GetArrayIndexOfNthSquareInMthBlock(automat, nth_square, mth_block);
    automat->block[index] = value;
}

/*
 * Sudoku_CalculateIndices
 * 
 * Ein Sudoku-Spielfeld ist ein 2-dimensionales Array bestehend aus 9x9 = 81 Kästchen
 * (geteilt in 9 Zeilen und 9 Spalten). Die Zeilen und Spalten sind jeweils von 0 bis 8 
 * nummeriert. Ein Kästchen in der Zeile r und Spalte c erhält den 2-dimensionalen Index 
 * (r,c). Dadurch hat z.B. ein Kästchen in der Zeile 2 und der Spalte 5 den Index (2,5).
 * 
 * In unserer Implementierung verwenden wir 1-dimensionales Array (anstatt 2-dimensionalem)
 * und bilden jeden 1-dimensionalen Kästchen-Index auf den entsprechenden 2-dimensionalen
 * (Zeilen,Spalten)-Index ab. Die 1-dimensionalen Kästchen-Indizes sind von 0 bis 80 nummeriert.
 * Durch folgende Formel wird jeder 1-dimensionalen Kästchen-Index <index> auf den passenden 
 * 2-dimensionalen (Zeilen,Spalten)-Index (row,col) abgebildet:
 * 
 *      index |==> (row, col) := (index / 9, index % 9)
 * 
 * bzw.
 * 
 *      row := index / 9
 *      col := index % 9
 * 
 * Wenn wir den Kästchen-Index <index> und/oder den (Zeilen,Spalten)-Index (row,col), können
 * wir den ersten (letzten) Kästchen-Index <row_begin> (<row_end>) einer Zeile sowie den
 * ersten (letzten) Kästchen-Index <col_begin> (<col_end>) einer Spalte bestimmen.
 * 
 * Der Index des ersten (row_begin) bzw. letzten (row_end) Kästchens in einer bestimmten
 * Zeile <row> lässt sich berechnen durch:
 * 
 *      row_begin := (index / 9) * 9 = row * 9
 *      row_end   := row_begin + 8
 * 
 * Ebeneso lässt sich der Index des ersten (row_begin) bzw. letzten (row_end) Kästchens 
 * in einer bestimmten Spalte <col> berechnen durch:
 * 
 *      col_begin := index % 9 = col
 *      col_end   := col_begin + (9 * 8) = col_begin * 72
 * 
 * In Sudoku werden je 9 Kästchen (square) zu einem 9er-Block zusammengefasst. Mit 81 Kästchen
 * hat jedes Sudoku 9 solche 9er-Blöcke. Jedes Block hat den Blockindex (block_row, block_col):
 * 
 *      block_row := row / 3
 *      block_col := col / 3
 *      (block_row, block_col) := (row / 3, col / 3)
 * 
 * wobei 0 <= <block_row> <= 2 und 0 <= <block_col> <= 2.
 * 
 * Es gibt 9 solche 9er-Blöcke. Dementsprechend sind die Blockindizes wie folgt:
 * 
 * Block 0 (erster Block)   : (0,0)
 * Block 1 (zweiter Block)  : (0,1)
 * Block 2 (dritter Block)  : (0,2)
 * Block 3 (vierter Block)  : (1,0)
 * Block 4 (fünfter Block)  : (1,1)
 * Block 5 (sechster Block) : (1,2)
 * Block 6 (siebter Block)  : (2,0)
 * Block 7 (achter Block)   : (2,1)
 * Block 8 (neunter Block)  : (2,2)
 * 
 * Der Index des ersten Kästchens in einem bestimmten Block mit (block_row, block_col) lässt
 * sich berechnen durch:
 * 
 *      block_begin := (block_row * 3 * 9) + (block_col * 3) = (block_row * 27) + (block_col * 3)
 */
SUDOKU_API int Sudoku_CalculateIndices (Sudoku_Automat * automat, /* Kästchen-Index */ int square_index, int * row, int * col, 
                    int * row_begin, int * row_end, int * col_begin, int * col_end, int * block_row, int * block_col, int * block_begin) {

    if (square_index >= SUDOKU_TOTAL_SIZE || square_index < 0) {
        fputs("\nERROR: Sudoku_CalculateIndices::The index <square_index> is out of bound. Exit.\n", stdout);
        return -1; // FALSE, ERROR
    }
    /*
    else if (automat->block[square_index] != 0) { // Feld ist bereits mit Zahl gefüllt. Nothing to do.
        printf("\nsquare_index: %d, value: %d, list is NULL: %i\n", square_index, automat->block[square_index], (automat->arrList[square_index] == NULL));
        Sudoku_Print(automat);
        fputs("\nWARNING: Sudoku_CalculateIndices::The square with index <square_index> is already filled with a dictinct number. Nothing to do.\n", stdout);
        return 0; // FALSE, WARNING
    }
    */
    *row = square_index / SUDOKU_LINE_SIZE;
    *col = square_index % SUDOKU_LINE_SIZE;
    *row_begin = *row * SUDOKU_LINE_SIZE;
    *row_end = *row_begin + (SUDOKU_LINE_SIZE - 1);
    *col_begin = *col;
    *col_end = *col_begin + SUDOKU_LINE_SIZE * (SUDOKU_LINE_SIZE - 1);
    *block_row = *row / SUDOKU_BLOCK_DIM;
    *block_col = *col / SUDOKU_BLOCK_DIM;
    *block_begin = (*block_row * SUDOKU_BLOCK_DIM * SUDOKU_LINE_SIZE) + (*block_col * SUDOKU_BLOCK_DIM);
    //
    return 1; // TRUE
}

/*
 * Sudoku_UpdateRelatedSquares
 * 
 * After a certain square value is changed/updated, all other squares in same (related) row, column and 3x3-block are
 * also updated such that there will be no duplicates in that related row, column and 3x3-block. Every value (number)
 * is unique in a row, column and 3x3-block.
 * 
 * @params
 *      automat :
 *      square_index : Array index of the current square
 *      count_empty_squares : Pointer to the variable that stores the number of all empty squares in sudoku board.
 *                            Whenever there is no empty square no, that means, a solution is found.
 */
SUDOKU_API void Sudoku_UpdateRelatedSquares (Sudoku_Automat * automat, int square_index, int * count_empty_squares) {
    if (*count_empty_squares == 0) {
        return; // EXIT
    }
    int row = 0, col = 0, row_begin = 0, row_end = 0, col_begin = 0, col_end = 0, block_row = 0, block_col = 0, block_begin = 0;
    int Result = Sudoku_CalculateIndices(automat, square_index, &row, &col, &row_begin, &row_end, &col_begin, &col_end, &block_row, &block_col, &block_begin);
    if (Result != 1) {
        fputs("\nERROR: Sudoku_UpdateRelatedSquares::There's problem with the index <square_index>. Program aborted.\n", stdout);
        return;
    }
    unsigned short int value = automat->block[square_index];
    /*** Update related row ***/
    for (int idx = row_begin; idx <= row_end && idx != square_index; idx++) {
        if (automat->block[idx] != 0) continue; // Square is NOT EMPTY
        int dst_index = 0;
        Node * node = LinkedList_GetNodeWithValue(automat->arrList[idx], value, &dst_index); // Seek for node with <value> from list <automat->arrList[idx]>
        if (node != NULL && dst_index < automat->arrList[idx]->size) { // If a node with value <automat->block[idx]> exists in list <automat->arrList[idx]>
            LinkedList_RemoveNodeByReference(automat->arrList[idx], node); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
            if (automat->arrList[idx]->size == 1) {
                automat->block[idx] = automat->arrList[idx]->begin->value;
                LinkedList_ClearList(automat->arrList[idx]);
                automat->arrList[idx] = NULL;
                --(*count_empty_squares);
                // Recursive call of [Sudoku_UpdateRelatedSquares]
                Sudoku_UpdateRelatedSquares (automat, idx, count_empty_squares);
            }
        }
    }
    /*** Update related column ***/
    for (int idx = col_begin; idx <= col_end && idx != square_index; idx += SUDOKU_LINE_SIZE) {
        if (automat->block[idx] != 0) continue; // Square is NOT EMPTY
        int dst_index = 0;
        Node * node = LinkedList_GetNodeWithValue(automat->arrList[idx], value, &dst_index); // Seek for node with value <automat->block[idx]> from list <automat->arrList[idx]>
        if (node != NULL && dst_index < automat->arrList[idx]->size) { // If a node with value <automat->block[idx]> exists in list <automat->arrList[idx]>
            if (automat->arrList[idx]->size == 1) { printf("DEBUG (col): %d\n", 1); }
            LinkedList_RemoveNodeByReference(automat->arrList[idx], node); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
            if (automat->arrList[idx]->size == 1) {
                automat->block[idx] = automat->arrList[idx]->begin->value;
                LinkedList_ClearList(automat->arrList[idx]);
                automat->arrList[idx] = NULL;
                --(*count_empty_squares);
                // Recursive call of [Sudoku_UpdateRelatedSquares]
                Sudoku_UpdateRelatedSquares (automat, idx, count_empty_squares);
            }
        }
    }
    /*** Update related 3x3-block ***/
    for (int j = 0; j < SUDOKU_BLOCK_DIM; j++) {
        for (int k = 0; k < SUDOKU_BLOCK_DIM; k++) {
            int idx = block_begin + j * SUDOKU_LINE_SIZE + k;
            if (automat->block[idx] != 0 || idx == square_index) continue; // Square is NOT EMPTY
            int dst_index = 0;
            Node * node = LinkedList_GetNodeWithValue(automat->arrList[idx], value, &dst_index); // Seek for node with value <automat->block[idx]> from list <automat->arrList[idx]>
            if (node != NULL && dst_index < automat->arrList[idx]->size) { // If a node with value <automat->block[idx]> exists in list <automat->arrList[idx]>
                if (automat->arrList[idx]->size == 1) { printf("DEBUG (block): %d\n", 1); }
                LinkedList_RemoveNodeByReference(automat->arrList[idx], node); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
                if (automat->arrList[idx]->size == 1) {
                    automat->block[idx] = automat->arrList[idx]->begin->value;
                    LinkedList_ClearList(automat->arrList[idx]);
                    automat->arrList[idx] = NULL;
                    --(*count_empty_squares);
                    // Recursive call of [Sudoku_UpdateRelatedSquares]
                    Sudoku_UpdateRelatedSquares (automat, idx, count_empty_squares);
                }
            }
        }
    }
}

/*
 * Sudoku_EliminateDuplicateCandidates
 *
 * Seek/Look in the row, column and block of the current square with index <square_index> for
 * squares with the same value as square with <square_index>. Remove these (duplicate) values 
 * from the list.
 * 
 * @params
 *      automat :
 *      square_index :
 */
SUDOKU_API int Sudoku_EliminateDuplicateCandidates (Sudoku_Automat * automat, int square_index) {
    int row = 0, col = 0, row_begin = 0, row_end = 0, col_begin = 0, col_end = 0, block_row = 0, block_col = 0, block_begin = 0;
    int Result = Sudoku_CalculateIndices(automat, square_index, &row, &col, &row_begin, &row_end, &col_begin, &col_end, &block_row, &block_col, &block_begin);
    if (Result != 1) {
        fputs("\nERROR: Sudoku_EliminateDuplicateCandidates::There's problem with the index <square_index>. Program aborted.\n", stdout);
        return -1;
    }
    /***
     * Look if a value in the list does exist in one of other squares in the related row, column or block.
     ***/
    if (automat->arrList[square_index] == NULL) return -1;
    Node * node = automat->arrList[square_index]->begin;
    do { // !!!!! We assume that <list->size> is exactly GREATER THAN 1
        int old_list_size = automat->arrList[square_index]->size;
        /*** Seek in related row ***/
        for (int idx = row_begin; idx <= row_end; idx++) {
            if (automat->block[idx] == node->value) {
                Node * node_to_remove = node;                
                node = node->next;
                LinkedList_RemoveNodeByReference(automat->arrList[square_index], node_to_remove); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
                break;
            }
        }
        if (automat->arrList[square_index]->size == old_list_size) { // No duplicate was found in related row. Continue with related column
            /*** Seek in related column ***/
            for (int idx = col_begin; idx <= col_end; idx += SUDOKU_LINE_SIZE) {
                if (automat->block[idx] == node->value) {
                    Node * node_to_remove = node;                
                    node = node->next;
                    LinkedList_RemoveNodeByReference(automat->arrList[square_index], node_to_remove); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
                    break;
                }
            }
            if (automat->arrList[square_index]->size == old_list_size)  { // No duplicate was found in related column. Continue with related 3x3-block
                /*** Seek in related 3x3-block ***/
                for (int j = 0; j < SUDOKU_BLOCK_DIM; j++) {
                    for (int k = 0; k < SUDOKU_BLOCK_DIM; k++) {
                        int idx = block_begin + j * SUDOKU_LINE_SIZE + k;
                        if (automat->block[idx] == node->value) {
                            Node * node_to_remove = node;                
                            node = node->next;
                            LinkedList_RemoveNodeByReference(automat->arrList[square_index], node_to_remove); // Remove <node> from list <automat->arrList[index]>. (list->size) is decreased.
                            k = j = SUDOKU_BLOCK_DIM; // Break condition for outer for-loop
                            break;
                        }
                    }
                }
            }
        }
        if (automat->arrList[square_index]->size == old_list_size) { // No duplicate was found. Continue with next <node>
            node = node->next;
        }
    } while (node); // node != NULL
    return automat->arrList[square_index]->size;
}

/*
 * Sudoku_SeekCandidates
 */
SUDOKU_API void Sudoku_SeekCandidates (Sudoku_Automat * automat, List * candiates_list, int square_index /* Kästchen-Index */) {
    int row = 0, col = 0, row_begin = 0, row_end = 0, col_begin = 0, col_end = 0, block_row = 0, block_col = 0, block_begin = 0;
    int Result = Sudoku_CalculateIndices(automat, square_index, &row, &col, &row_begin, &row_end, &col_begin, &col_end, &block_row, &block_col, &block_begin);
    if (Result == -1) {
        fputs("\nERROR: Sudoku_SeekCandidates::There's problem with the index <square_index>. Program aborted.\n", stdout);
        return;
    }
    /***
     * Seek values for the empty square
     ***/
    for  (unsigned short int value = 1; value <= SUDOKU_LINE_SIZE; value++) {
        int value_not_exist = 1; // TRUE <=> value (1..9) does not exist in current row/column/block
        /*** Seek in related row ***/
        for (int idx = row_begin; idx <= row_end; idx++) {
            if (automat->block[idx] == value) {
                value_not_exist = 0; // FALSE
                break;
            }
        }
        /*** Seek in related column ***/
        if (value_not_exist) {
            for (int idx = col_begin; idx <= col_end; idx += SUDOKU_LINE_SIZE) {
                if (automat->block[idx] == value) {
                    value_not_exist = 0; // FALSE
                    break;
                }
            }
        }
        /*** Seek in related 3x3-block ***/
        if (value_not_exist) {
            for (int j = 0; j < SUDOKU_BLOCK_DIM; j++) {
                for (int k = 0; k < SUDOKU_BLOCK_DIM; k++) {
                    int idx = block_begin + j * SUDOKU_LINE_SIZE + k;
                    if (automat->block[idx] == value) {
                        value_not_exist = 0; // FALSE
                        k = j = SUDOKU_BLOCK_DIM;
                        break;
                    }
                }
            }
        }
        /*** Insert value in to list iff it still does not exist in the sudoku squares ***/
        if (value_not_exist) {
            LinkedList_AddNodeWithValue(candiates_list, value);
        }
    } // End for
}

/*
 * Sudoku_Solve
 * 
 * Provide a valid solution for the given sudoku puzzle.
 *
 * @params
 *      automat : Sudoku automaton for solving and/or generating puzzle(s)
 */
SUDOKU_API void Sudoku_Solve (Sudoku_Automat * automat) {
    /* 
     * Jedes Sudoku-Kästchen kann mögliche Werte zwischen 1 und 9 (1,9 eingeschlossen) annehmen.
     * Diese möglichen Werte werden in einer verketteten Liste aufgenommen. Da es insgesamt 9x9,
     * also 81, Kästchen gibt, gibt es auch 81 solche Listen. Konkrett bedeutet es, dass jedes
     * Kästchen eine Liste enthält => Array der Listen. Am Ende des Programms sollen alle Listen
     * im Array jeweils höchstens nur einen Knoten (für gesuchte Zahl) enthalten.
     */

    /*** Count the number of empty squares ***/
    int count_empty_squares = 0;
    for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
        if (automat->block[index] == 0) { // empty square
            count_empty_squares += 1;
        }
    }
    
    /*** Fill all empty squares with candidates (list) ***/
    for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
        automat->arrList[index] = NULL; // Wenn dieses Feld bereits mit einer eindeutigen Zahl gefüllt ist.
        if (automat->block[index] != 0) { // (automat->block[index]) != 0
            continue;
        }
        automat->arrList[index] = LinkedList_CreateList(); // Create a new value list for current square <index>
        Sudoku_SeekCandidates(automat, automat->arrList[index], index);
        if (automat->arrList[index]->size == 1) {
            automat->block[index] = automat->arrList[index]->begin->value;
            LinkedList_ClearList(automat->arrList[index]);
            automat->arrList[index] = NULL;
            //--count_empty_squares;
            if ((--count_empty_squares) == 0) break;
        }
    }

    /*** Update all sudoku squares until there is no empty squares more and every square has only one distinct value (1..9) ***/
    while (count_empty_squares > 0) {
        for (int index = 0; index < SUDOKU_TOTAL_SIZE; index++) {
            if (automat->block[index] != 0 && automat->arrList[index] == NULL) continue; // Dieses Feld ist bereits mit einer eindeutigen Zahl gefüllt.
            // Seek duplicates in the related row, column and/or 3x3 block. If exists, then removes the duplicate in list.
            int list_size = Sudoku_EliminateDuplicateCandidates (automat, index);
            if (list_size == 1) {
                automat->block[index] = automat->arrList[index]->begin->value;
                LinkedList_ClearList(automat->arrList[index]);
                automat->arrList[index] = NULL;
                //--count_empty_squares;
                if ((--count_empty_squares) == 0) break;
                // Recursive update other squares in related row, column and 3x3 block
                Sudoku_UpdateRelatedSquares(automat, index, &count_empty_squares);
            }
        }
    }
}

/*
 * Sudoku_Generate
 * 
 * Provide a valid/solvable sudoku puzzle.
 *
 * @params
 *      automat : Pointer to an sudoku automaton for solving and/or generating puzzle(s)
 */
SUDOKU_API void Sudoku_Generate (Sudoku_Automat * automat) {
    ////
    // Initialization. Seeds the pseudo-random number generator with time value as seed. Called only once.
    ////
    srand(time(0));
    ////
    // Choose a random block with index within interval [0..8]
    ////
    int block_index = rand() % SUDOKU_BLOCK_NUMBER; // Randomly choose a 3x3-block with index 0..8
    ////
    //  Choose from this block a random square with index within interval [0..8]
    ////
    int square_index = rand() % SUDOKU_NUMBER_SQUARES_IN_BLOCK;
    ////
    // Write a value (0..8) to this square of this block
    ////
    unsigned short int value = ((unsigned short int) rand()) % SUDOKU_NUMBER_SQUARES_IN_BLOCK;
    Sudoku_WriteValueToNthSquareInMthBlock(automat, square_index, block_index, value);
    ////
    //
    ////
    int start_block_index = block_index; // Remember the index of the first chosen block
    block_index = (block_index + 1) % SUDOKU_BLOCK_NUMBER; // Update to the next block index
    do {

    }
    while (block_index != start_block_index);
}

/*
 * Sudoku_Run
 * 
 * Execute the automaton to either solve a given sudoku puzzle or generate a new sudoku puzzle.
 * 
 * @params
 *      automat : Pointer to an sudoku automaton for solving and/or generating puzzle(s)
 */
SUDOKU_API void Sudoku_Run (Sudoku_Automat * automat) {
    if (automat->mode == SUDOKU_MODE_SOLVER) {
        // Solves the given sudoku puzzle
        Sudoku_Solve(automat);
    }
    else if (automat->mode == SUDOKU_MODE_GENERATOR) {
        // Creates a (new) sudoku puzzle
        Sudoku_Generate(automat);
    }
}

/*
 * Sudoku_ChangeMode
 * 
 * Switch between two working modes SUDOKU_MODE_SOLVER and SUDOKU_MODE_GENERATOR.
 * The first mode SUDOKU_MODE_SOLVER causes the automaton to solve a given sudoku
 * puzzle. The second mode SUDOKU_MODE_GENERATOR creates a (new) sudoku puzzle.
 * 
 * @params
 *      automat  : Pointer to an sudoku automaton
 *      new_mode : Mode to switch to (SUDOKU_MODE_SOLVER or SUDOKU_MODE_GENERATOR)
 * 
 */
SUDOKU_API void Sudoku_ChangeMode (Sudoku_Automat * automat, Sudoku_Automat_Mode new_mode) {
    automat->mode = new_mode;
}

/*
 * Sudoku_Init
 * 
 * Initialize the automaton with the desired mode (SUDOKU_MODE_SOLVER or SUDOKU_MODE_GENERATOR).
 * 
 * @params
 *      automat : Pointer to an sudoku automaton
 *      mode    : Working mode (SUDOKU_MODE_SOLVER or SUDOKU_MODE_GENERATOR)
 * 
 */
SUDOKU_API void Sudoku_Init (Sudoku_Automat * automat, Sudoku_Automat_Mode mode, char * __restrict__ puzzle_file) {
    automat->mode = mode;
    automat->block = (unsigned short int *) malloc(sizeof(unsigned short int) * SUDOKU_TOTAL_SIZE);
    ////
    // SUDOKU SOLVING MODE
    ////
    if (mode == SUDOKU_MODE_SOLVER) {
        /**** Read puzzle file and update sudoku block ****/
        FILE * fp;
        int m = 0; // Line number counter
        char line[SUDOKU_LINE_SIZE + 2]; // 9 numbers plus '\n' (new-line) plus '\0' (null-terminated character)
        //
        /**** Open file stream/pointer ****/
        fp = fopen(puzzle_file, "r");
        if (fp == NULL) { 
            fprintf(stderr, "Error (Sudoku_Init): fopen::Cannot open file %s.\n", puzzle_file); 
            exit(EXIT_FAILURE);
        }
        //
        /**** Read line by line and fill the sudoku block ****/
        while (fgets(line, SUDOKU_LINE_SIZE + 2, fp)) {
            for (int i = 0; i < SUDOKU_LINE_SIZE; i++) {
                if (line[i] >= '1' && line[i] <= '9') {
                    automat->block[m * SUDOKU_LINE_SIZE + i] = (unsigned short int) (line[i] - '0'); // Insert value between 1 and 9 (included)
                }
                else {
                    automat->block[m * SUDOKU_LINE_SIZE + i] = 0; // Insert 0 if read value is not between 1 and 9
                }
            }
            m += 1;
        }
        //
        /**** Close file stream ****/
        if (fclose(fp) != 0) { 
            fprintf(stderr, "Error (Sudoku_Init): fclose::Cannot close file %s.\n", puzzle_file); 
            exit(EXIT_FAILURE);
        }
    }
    ////
    // SUDOKU GENERATING MODE
    ////
    else if (mode == SUDOKU_MODE_GENERATOR) {
        /**** Prepare array for storing sudoku numeric values ****/
        for (int i = 0; i < SUDOKU_TOTAL_SIZE; ++i)
            automat->block[i] = 0;
    }
    ////
    // INVALID MODE
    ////
    else {
        fprintf(stderr, "Error (Sudoku_Init): Invalid mode. There are only two valid modes (SUDOKU_MODE_SOLVER and SUDOKU_MODE_GENERATOR).\n");
        exit(EXIT_FAILURE);
    }
}

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* SUDOKU_H */