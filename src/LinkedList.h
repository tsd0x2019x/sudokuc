/*
 * LinkedList.h
 * 
 * This header file provides an simple implementation of linked-list and its helper functions.
 * 
 */
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#define LinkedList_API extern

//=======================================================================================================================
typedef struct node_t Node; // forward-declaration
struct node_t {
    unsigned short int value;
    Node * next;
};

typedef struct {
    unsigned int size;
    Node * begin;
    Node * end;
} List;


//=======================================================================================================================

LinkedList_API int LinkedList_AddNode (List * list, Node * new_node);
LinkedList_API int LinkedList_AddNodeWithValue (List * list, int value);
LinkedList_API List * LinkedList_CreateList ();
LinkedList_API void LinkedList_ClearList (List * list);
LinkedList_API Node * LinkedList_GetNodeWithIndex (List * list, int index);
LinkedList_API Node * LinkedList_GetNodeWithValue (List * list, int value, int * dst_index);
LinkedList_API int LinkedList_RemoveNodeWithValue (List * list, int value);
LinkedList_API int LinkedList_RemoveNodeByReference (List * list, Node * node_to_remove);
LinkedList_API void LinkedList_PrintList (List * list);

//=======================================================================================================================
/*
 * Add new node at the list end. Return 0 if list is NULL, i.e. no list exists. Otherwise, return (updated) list size.
 */
LinkedList_API int LinkedList_AddNode (List * list, Node * new_node) {
    if (list == NULL) return 0; // There is no list => Nothing to add.
    if (list->size == 0) { // Empty list.
        list->begin = list->end = new_node;
    }
    else {
        list->end->next = new_node;
        list->end = new_node;
    }
    list->end->next = NULL;
    return ++(list->size);
}

/*
 * Add new node with [value] at the list end. Return 0 if no list exists (NULL). Otherwise, return (updated) list size.
 */
LinkedList_API int LinkedList_AddNodeWithValue (List * list, int value) {
    if (list == NULL) return 0; // There is no list => Nothing to add.
    Node * new_node = (Node *) malloc(sizeof(Node));
    new_node->value = value;
    return LinkedList_AddNode(list, new_node);
}

/*
 * Allocate memory to create a new (empty) list object and return a pointer to it.
 */
LinkedList_API List * LinkedList_CreateList () {
    List * list = (List *) malloc(sizeof(List));
    list->begin = list->end = NULL;
    list->size = 0;
    return list;
}

/*
 * Free all allocated memory for the list object and its nodes.
 */
LinkedList_API void LinkedList_ClearList (List * list) {
    if (list) {
        if (list->size > 0) {
            Node * next = NULL;
            do {
                next = list->begin->next;
                list->begin->next = NULL;
                free(list->begin);
                --(list->size);
                list->begin = next;
            }
            while (next);
            list->end = NULL;
        }
        free(list);
        list = NULL;
    }
}

/*
 * Provides a reference of the node at index [index] in the list. If the list does not exist (is NULL) or empty
 * (size = 0), a NULL pointer is returned. If index is smaller than zero (0) or bigger equal to list size, NULL
 * pointer is returned as well.
 */
LinkedList_API Node * LinkedList_GetNodeWithIndex (List * list, int index) {
    if (list == NULL || list->size == 0 || index < 0 || index >= list->size) {
        return NULL;
    }
    Node * node = list->begin;
    while (index > 0) {
        index--;
        node = node->next;
    }
    return node;
}

/*
 *
 */
LinkedList_API Node * LinkedList_GetNodeWithValue (List * list, int value, int * dst_index) {
    if (list == NULL || list->size == 0) {
        if (dst_index != NULL) *dst_index = -1;
        return NULL;
    }
    Node * node = list->begin;
    int i = 0;
    do {
        if (node->value == value) {
            break;
        }
        ++i;
    } while ((node=node->next));
    if (dst_index != NULL) *dst_index = i;
    return node;
}

/*
 * Remove the node with (the first occurence of) value [value]. Return -1 if no list eixsts or list is being empty.
 * Otherwise, return (updated) list size.
 */
LinkedList_API int LinkedList_RemoveNodeWithValue (List * list, int value) {
    if (list == NULL || list->size == 0) return -1;
    if (list->size == 1) {
        if (list->begin->value == value) {
            free(list->begin);
            list->begin = list->end = NULL;
            --(list->size);
        }        
    }
    else { // list->size > 1
        int index = 0;
        Node * node = LinkedList_GetNodeWithValue(list, value, &index); // Current node to be removed
        if (index > -1 && index < list->size) { // Node with [value] does exist in list
            if (index == 0) { // First node: node == list->begin
                Node * tmp = node->next; // = list->begin->next;
                node->next = NULL; // list->begin->next = NULL
                list->begin = tmp;
            }
            else { // index > 0
                Node * prev = LinkedList_GetNodeWithIndex(list, index-1); // Previous node
                if (index == list->size - 1) { // Last node
                    list->end = prev;
                }                
                prev->next = node->next;
                node->next = NULL;
            }
            free(node);
            node = NULL;
            --(list->size);
        }
    }
    return list->size;
}

/*
 * Remove the node referred by the pointer <node> from list. Attention: The pointer <node_to_remove>
 * must really point to a node in the list. Return the updated list size. If the node <node_to_remove>
 * is not in the list, nothing is removed and the list-size does not change.
 */
LinkedList_API int LinkedList_RemoveNodeByReference (List * list, Node * node_to_remove) {
    if (list == NULL || list->size == 0) return -1;
    if (list->size == 1) {
        if (list->begin == node_to_remove) {
            free(list->begin);
            list->begin = list->end = node_to_remove = NULL;
            --(list->size);
        }
    }
    else if (list->size > 1) {
        if (list->begin == node_to_remove) {
            list->begin = node_to_remove->next;
            node_to_remove->next = NULL;
            free(node_to_remove);
            node_to_remove = NULL;
            --(list->size);
        }
        else {
            Node * prev = list->begin; // The "previous" node that is in left of <node_to_remove>
            do {
                if (prev->next == node_to_remove) {
                    prev->next = node_to_remove->next;
                    node_to_remove->next = NULL;
                    if (node_to_remove == list->end) {
                        list->end = prev;
                    }
                    free(node_to_remove);
                    node_to_remove = NULL;
                    --(list->size);
                    break;
                }
            } while ((prev = prev->next) != NULL);
        }
    }
    return list->size;
}

/*
 * Display (all nodes of) the list.
 */
LinkedList_API void LinkedList_PrintList (List * list) {
    if (list == NULL) printf("NULL\n");
    else if (list->size == 0) printf("Empty: ()"); // Empty list
    else {
        int idx = 0;
        Node * node = list->begin;
        printf("(%hu", node->value);
        while (idx < (list->size-1)) {
            ++idx;
            node = node->next;
            printf("->%hu", node->value);
        }
        printf(")\n");
    }
}

#ifdef __cplusplus
}
#endif

#endif /* LINKEDLIST_H */