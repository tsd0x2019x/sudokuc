#!/bin/bash

MAIN_DIR=$(pwd)
FILE_NAME="Puzzle.txt"

rm -rf ./bin/*  # Remove all (old) executable and Puzzle*.txt files in ./bin/

make clean && make

cp ./puzzles/*.txt ./bin/  # Copy all Puzzle*.txt files from ./puzzles/ to ./bin/

cd ./bin/  && ./Sudoku ${FILE_NAME}

cd ${MAIN_DIR}
