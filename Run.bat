@ECHO OFF
@REM =========================================
@REM    Run.bat
@REM ==========================================
@SET BINARY=Sudoku64
@SET SRC_FILE=Main.c
@SET SRC_DIR=src
@SET BIN_DIR=bin
@SET MAIN_DIR=%CD%
@SET PUZZLE_DIR=puzzles
@SET INPUT=Puzzle.txt
@REM "x86_64-w64-mingw32-gcc.exe" is the equivalent 
@REM of "gcc.exe" in Windows.
@SET MINGW64=x86_64-w64-mingw32-gcc.exe
@SET OPTIONS=-Wall -Werror -std=gnu99
@SET LINKOPT=-lm
@REM Create folder "bin\"
@MKDIR %BIN_DIR%
@REM Delete all .exe files and subfolders in "bin/".
@REM Parameter /F forces to remove all protected files,
@REM /S to remove all files in all subfolders and
@REM /Q to disable the query/prompt if using place holder
@REM like "*" etc.
@DEL /F /S /Q %BIN_DIR%\%BINARY%.exe
@REM Delete all Puzzle*.txt files in "bin/"
@DEL /F /S / Q %BIN_DIR%\Puzzle*.txt
@REM Go back to main directory
@CD %MAIN_DIR%
@REM Copy all (updated) Puzzle*.txt files from "puzzles\" to "bin\"
@XCOPY /Y %PUZZLE_DIR%\*.txt %BIN_DIR%\
@REM Compile for 64 bit
@%MINGW64% %OPTIONS% %SRC_DIR%\%SRC_FILE% -o %BIN_DIR%\%BINARY%.exe %LINKOPT%
@ECHO Built 64 bit binray for Sudoku.
@ECHO Done.
@REM
@REM TODO:
@REM Configure MinGW and GCC to compile 32 bit
@REM binary on a 64 bit system.
@ECHO
@ECHO Start program
@CD %BIN_DIR%\
@%BINARY%.exe %INPUT%
@CD %MAIN_DIR%