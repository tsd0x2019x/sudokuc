@ECHO OFF
@REM =========================================
@REM    Clean_Bin.bat
@REM ==========================================
@SET BIN_DIR=bin
@SET MAIN_DIR=%CD%
@REM Remove all existing files in folder "bin\".
@REM /F forces to remove all protected files,
@REM /S to remove all files in all subfolders and
@REM /Q to disable the query/prompt if using place holder like "*" etc.
@DEL /F /Q /S %BIN_DIR%\*
@RMDIR /S /Q %BIN_DIR%