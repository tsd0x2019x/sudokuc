#==============================================================================================
# Directories and files
#==============================================================================================
SRC_DIR := ./src/
OUT_DIR := ./bin/

SRC_FILE_H	:= $(SRC_DIR)Sudoku.h
SRC_FILE_C 	:= $(SRC_DIR)Main.c
OUT_FILE	:= $(OUT_DIR)Sudoku

#==============================================================================================
# Compiler
#==============================================================================================
CC_GCC		:= /usr/bin/gcc # GCC standard compiler for Linux
CC_MINGW32  := /usr/bin/i686-w64-mingw32-gcc # MinGW32 compiler for Windows 32-bit *.exe
CC_MINGW64  := /usr/bin/x86_64-w64-mingw32-gcc # MinGW64 compiler for Windows 64-bit *.exe
CC 			:= $(CC_GCC)

# Flags
CFLAGS  	:= -Wall -Werror -std=gnu99 -pedantic # Compiler flags
LDFLAGS 	:= -lm # Linker flags

#==============================================================================================
# Commands
#==============================================================================================
MKDIR_P 	:= mkdir -p
RM_F		:= rm -f

#==============================================================================================
# Rules
#==============================================================================================
.PHONY: build clean

build:	# Type "make build" in console to build/compile executable from source.
	$(MKDIR_P) $(OUT_DIR)
	$(CC) $(CFLAGS) $(SRC_FILE_C) -o $(OUT_FILE) $(LDFLAGS)
	$(CC_MINGW64) $(CFLAGS) $(SRC_FILE_C) -o $(OUT_FILE).exe $(LDFLAGS)

clean:	# Type "make clean" in console to remove all executable files in $(OUT_DIR).
	$(RM_F) $(OUT_FILE) $(OUT_FILE).exe

#==============================================================================================
# Main program
#==============================================================================================
main:	# Type "make" in console to run the build process. Similar to "make build".
	build