# SudokuC

This is a Sudoku solver and generator for 9x9 grid (with nine 3x3 subgrids).

==============================================================================

# HOW TO QUICK-COMPILE AND TEST

==============================================================================

The most easiest way is to (A) run the Shell script "Run.sh" in Linux:

        $ ./Run.sh

or (B) run the Batch script "Run.bat" in Windows:

        $ Run.bat

This script will do the following internal steps:
    (1) Delete all (old) files in folder "bin/",
    (2) Copy the puzzle files from "puzzles/" to "bin/",
    (3) Compile source file to executable (binary) file (Sudoku or Sudoku.exe)
    (4) Change to "bin/", run the executable (Sudoku or Sudoku.exe) and read 
        the "Puzzle.txt" file, that contains an unsolved sudoku puzzle,
    (5) Display the solution of this sudoku puzzle.

==============================================================================

# HOW TO USE THE BINARY (OR EXECUTABLE)

==============================================================================

After compilation and obtaining an executable (binary) file, e.g. Sudoku or 
Sudoku.exe, one can run/test the program as follows:

    $ ./Sudoku Puzzle.txt

or

    & Sudoku.exe Puzzle.txt

==============================================================================

# HOW TO CROSS-COMPILE (GENERATE binary and/or .exe file) FOR LINUX AND WINDOWS

==============================================================================

I do not compile source immediately in Windows, but in Linux (Ubuntu).

For that purpose, I installed the package Mingw-w64 (Linux):

    $ sudo apt-get install mingw-w64

and use it to cross-compile for both Linux and Windows. 

After installation of Mingw-w64, one can just type "make" or "make build" to 
build the program (compile & link):

    $ make

or

    $ make build

In the Makefile, all options for cross-compiling are made. It creates two files:

    a) "Sudoku" for Linux, and

    b) "Sudoku.exe" for Windows

Both files are in the subfolder "bin/" within the Sudoku working directory.

All source files are found in the "src/" subfolder within the Sudoku directory.

One can type "make clean" to remove the (old) binaries and files, in order to

prepare for new build process.

        $ make clean

==============================================================================

# ADDITIONAL INFORMATION

==============================================================================

INSTALL MINGW-W64 IN LINUX

1. Install Mingw-w64 (Linux):

    $ sudo apt-get install mingw-w64

2. Compile in Linux using GCC:

    $ /usr/bin/gcc

 3. Cross-compile for Windows using Mingw-w64:
 
    3.1. For 32-Bit: 

        $ /usr/bin/i686-w64-mingw32-gcc <progname.c> -o <progname>

    3.2. For 64-Bit:

        $ /usr/bin/x86_64-w64-mingw32-gcc <progname.c> -o <progname>
		
==============================================================================

INSTALL MINGW-W64 IN WINDOWS

1. Go to http://mingw-w64.org/doku.php/download

2. Download MinGW-W64 for Windows OS

3. Set the path to "bin/" folder of the installed MinGW-W64 as global.
   (In "bin/" are the files "i686-w64-mingw32-gcc.exe" and "x86_64-w64-mingw32-gcc")
   
4. Compile/Build the executable for x86 and 64bit version Windows:

	4.1. For 32-Bit: 

        $ i686-w64-mingw32-gcc <progname.c> -o <progname>

    4.2. For 64-Bit:

        $ x86_64-w64-mingw32-gcc <progname.c> -o <progname> 
